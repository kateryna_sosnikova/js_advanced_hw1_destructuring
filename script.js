const cities = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
const [city1, city2, city3, city4, city5] = cities;
console.log(city1);
console.log(city2);
console.log(city3);
console.log(city4);
console.log(city5);


let Employee = {
    name: 'Zoe', 
    salary: 2500,
}

const {name, salary} = Employee;
console.log(name, salary);




const array = ['value', 'showValue'];
const [value, showValue] = array;

alert(value); // будет выведено 'value'
alert(showValue);  // будет выведено 'showValue'